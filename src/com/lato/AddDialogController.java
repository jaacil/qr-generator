package com.lato;

import com.lato.datamodel.QRData;
import com.lato.datamodel.QRItem;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;


public class AddDialogController {

    @FXML
    TextField nameField;

    @FXML
    TextField surnameField;

    @FXML
    TextField titleField;

    @FXML
    TextField mobileField;

    @FXML
    TextField officeField;

    @FXML
    TextField emailField;

    public QRItem processResults() {
        String name = nameField.getText();
        String surname = surnameField.getText();
        String title = titleField.getText();
        String tel1 = mobileField.getText();
        String tel2 = officeField.getText();
        String email = emailField.getText();

        QRItem qrItem = new QRItem(name, surname, title, tel1, tel2, email);
        QRData.getInstance().addQrItem(qrItem);
        return qrItem;
    }

}
