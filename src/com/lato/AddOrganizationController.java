package com.lato;

import com.lato.datamodel.Organization;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

public class AddOrganizationController {

    @FXML
    TextField nameOrgField;

    @FXML
    TextField addressOrgField;

    @FXML
    TextField websiteOrgField;

    public Organization processResults() {
        if((nameOrgField.getText().trim().isEmpty()) || (addressOrgField.getText().trim().isEmpty()) || (websiteOrgField.getText().trim().isEmpty())) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Empty values!");
            alert.setContentText("You have to enter all values!");
            alert.showAndWait();
            return null;
        } else {
            String name = nameOrgField.getText();
            String address = addressOrgField.getText();
            String website = websiteOrgField.getText();
            return new Organization(name, address, website);
        }
    }

}
