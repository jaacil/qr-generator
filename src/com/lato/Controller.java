package com.lato;

import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.lato.datamodel.Organization;
import com.lato.datamodel.QRData;
import com.lato.datamodel.QRItem;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.util.Callback;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.function.Predicate;

import com.google.zxing.*;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public class Controller {

    @FXML
    private ListView<QRItem> qrListView;

    @FXML
    private TextArea itemDetailsTextArea;

    @FXML
    private BorderPane mainBorderPane;

    @FXML
    private ContextMenu listContextMenu;

    @FXML
    private Button generateQrButton;

    @FXML
    private Button generateOneQrButton;

    @FXML
    private Button newDataButton;

    @FXML
    private Button addOrganization;

    @FXML
    private Label titleLabel;

    @FXML
    private Label organizationLabel;

    private final Logger logger = Logger.getLogger(Controller.class.getName());

    private List<QRItem> qrItemList;
    private Predicate<QRItem> allQrItems;
    private String savePath;
    private static final String charset = "UTF-8";
    private Organization organization = null;

    public void initialize() {

        try {
            QRData.getInstance().openFromFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        qrListView.setItems(QRData.getInstance().getQrItems());
        qrListView.setCellFactory(new Callback<ListView<QRItem>, ListCell<QRItem>>() {
            @Override
            public ListCell<QRItem> call(ListView<QRItem> qrItemListView) {
                ListCell<QRItem> cell = new ListCell<>() {
                    @Override
                    protected void updateItem(QRItem qrItem, boolean b) {
                        super.updateItem(qrItem, b);
                        if (b) {
                            setText(null);
                        } else {
                            setText(qrItem.getName() + " " + qrItem.getSurname());
                        }
                    }
                };
                cell.emptyProperty().addListener(
                        (observableValue, wasEmpty, isEmpty) -> {
                            if (isEmpty) {
                                cell.setContextMenu(null);
                            } else {
                                cell.setContextMenu(listContextMenu);
                            }
                        });
                return cell;
            }
        });

        listContextMenu = new ContextMenu();

        MenuItem menuDelete = new MenuItem("Delete");
        menuDelete.setOnAction(actionEvent -> {
            QRItem qrItem = qrListView.getSelectionModel().getSelectedItem();
            deleteItem(qrItem);
        });

//        MenuItem menuOpen = new MenuItem("Open");
//        menuOpen.setOnAction(actionEvent -> {
//        });

        listContextMenu.getItems().addAll(menuDelete);

        qrListView.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            if (newValue != null) {
                QRItem qrItem = qrListView.getSelectionModel().getSelectedItem();
                itemDetailsTextArea.setText(" Phone: " +
                        qrItem.getTel1() + "\n Work: " +
                        qrItem.getTel2() + "\n Email: " +
                        qrItem.getEmail()); //detale
                titleLabel.setText(qrItem.getTitle());
            }
        });

        allQrItems = qrItem -> true;

        qrListView.getSelectionModel().selectFirst();
        qrListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

    }

    @FXML
    public void handleAddOrganizationButton() {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner(mainBorderPane.getScene().getWindow());
        dialog.setTitle("Add new organization");
        dialog.setHeaderText("Use this dialog to create a new organization");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("addOrg.fxml"));

        try {
            dialog.getDialogPane().setContent(fxmlLoader.load());
        } catch (IOException e) {
            logger.log(Level.WARNING, "Couldn't load the dialog");
            e.printStackTrace();
            return;
        }

        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);

        Optional<ButtonType> result = dialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            AddOrganizationController controller = fxmlLoader.getController();
            if (controller.processResults() != null) {
                organization = controller.processResults();
                organizationLabel.setText(organization.toString());
            } else {
                logger.log(Level.WARNING, "Organization is null");
            }
        }
    }


    @FXML
    public void showNewQrItemDialog() {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner(mainBorderPane.getScene().getWindow());
        dialog.setTitle("Add new contact");
        dialog.setHeaderText("Use this dialog to create a new contact item.");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("addItemDialog.fxml"));
        try {
            dialog.getDialogPane().setContent(fxmlLoader.load());
        } catch (IOException e) {
            logger.log(Level.WARNING, "Couldn't load the dialog");
            e.printStackTrace();
            return;
        }

        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);

        Optional<ButtonType> result = dialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            AddDialogController controller = fxmlLoader.getController();
            QRItem newItem = controller.processResults();
            qrListView.setItems(QRData.getInstance().getQrItems());
            qrListView.getSelectionModel().select(newItem);
        } else {
        }
    }

    @FXML
    public void handleKeyPressed(KeyEvent keyEvent) {
        QRItem selectedItem = qrListView.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            if (keyEvent.getCode().equals(KeyCode.DELETE)) {
                deleteItem(selectedItem);
            }
        }
    }

    @FXML
    public void handleClickedListView() {
        QRItem selectedItem = qrListView.getSelectionModel().getSelectedItem();
        itemDetailsTextArea.setText(selectedItem.getTitle());
    }

    @FXML
    public void handleGenerateOneButton() throws IOException, WriterException {

        QRItem selectedItem = qrListView.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            String filename = selectedItem.getName() + "_" + selectedItem.getSurname();

            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Place to save QR");
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PNG Files (*.png)", "*.png");
            fileChooser.getExtensionFilters().add(extFilter);
            fileChooser.setInitialFileName(filename);
            File file = fileChooser.showSaveDialog(null);

            if (file != null) {
                String code = buildQrCode(selectedItem);
                if(!code.equals("")) {
                    generateQRcode(code, file.getPath(), hashtable(), 200, 200);
                    logger.log(Level.INFO, "QR Code created successfully.");
                } else {
                    logger.log(Level.WARNING, "Organization or item is empty");
                }
            }
        } else {
            logger.log(Level.WARNING, "Item to generate is empty.");
        }
    }

    @FXML
    public void handleGenerateButton() throws IOException, WriterException {

        ObservableList<QRItem> items = QRData.getInstance().getQrItems();
        QRItem item;
        if (items != null) {
            DirectoryChooser chooser = new DirectoryChooser();
            chooser.setTitle("Place to save all QR codes");
            File directory = chooser.showDialog(null);

            if (directory != null) {
                for (QRItem qrItem : items) {
                    item = qrItem;
                    String code = buildQrCode(item);
                    String filename = directory.getAbsolutePath() + "\\" + item.getName() + "_" + item.getSurname() + ".png";
                    if(!code.equals("")) {
                        generateQRcode(code, filename, hashtable(), 200, 200);
                    } else {
                        logger.log(Level.WARNING, "Organization or items are empty");
                        Alert alert ;
                        break;
                    }
                    logger.log(Level.INFO, "All QR Codes were created successfully.");
                }
            }
        } else {
            logger.log(Level.WARNING, "Items to generate are empty.");
        }
    }

    @FXML
    public void handleEdit() {

    }

    @FXML
    public void handleNewData() {
        try {
            if (QRData.getInstance().openFromFile()) {
                itemDetailsTextArea.setText("");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        qrListView.setItems(QRData.getInstance().getQrItems());
        qrListView.getSelectionModel().selectFirst();
    }

    @FXML
    public void handleExit() {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Save file");
        alert.setHeaderText("Do you want to save file as separated txt file?");
        alert.setContentText("Press OK to confirm, or cancel to exit without saving.");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            saveAsTxt();
        }
        Platform.exit();
    }

    public Hashtable hashtable() {
        Hashtable hints = new Hashtable();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        return hints;
    }

    public String buildQrCode(QRItem selectedItem) {

        if (selectedItem != null && organization != null) {
            String name = selectedItem.getName();
            String surname = selectedItem.getSurname();
            String title = selectedItem.getTitle();
            String tel1 = selectedItem.getTel1();
            if (tel1.equals("Brak")) {
                tel1 = "";
            }
            String tel2 = selectedItem.getTel2();
            if (tel2.equals("Brak")) {
                tel2 = "";
            }
            String email = selectedItem.getEmail();

            String organizationName = organization.getName();
            String organizationAddress = organization.getAddress();
            String organizationWebsite = organization.getWebsite();

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.setLength(0);

            stringBuilder
                    .append("BEGIN:VCARD").append("\n")
                    .append("N:").append(surname).append(";").append(name).append(";").append("\n")
                    .append("TEL;TYPE=work,VOICE:").append(tel2).append("\n")
                    .append("TEL;TYPE=home,VOICE:").append(tel1).append("\n")
                    .append("EMAIL:").append(email).append("\n")
                    .append("ORG:").append(organizationName).append("\n")
                    .append("TITLE:").append(title).append("\n")
                    .append("ADR;TYPE=WORK,PREF:").append(organizationAddress).append("\n")
                    .append("URL:").append(organizationWebsite).append("\n")
                    .append("VERSION:3.0").append("\n")
                    .append("END:VCARD");

            return stringBuilder.toString();
        } else {
            logger.log(Level.WARNING, "Organization or item is null!");
            return "";
        }
    }

    public void saveAsTxt() {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT Files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showSaveDialog(null);

        try {
            QRData.getInstance().storeQrItems(file.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void generateQRcode(String data, String path, Map map, int h, int w) throws WriterException, IOException {
        BitMatrix matrix = new MultiFormatWriter().encode(data, BarcodeFormat.QR_CODE, w, h, map);
        MatrixToImageWriter.writeToFile(matrix, path.substring(path.lastIndexOf('.') + 1), new File(path));
    }

    public static String readQRcode(String path) throws FileNotFoundException, IOException, NotFoundException {
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(ImageIO.read(new FileInputStream(path)))));
        Result rslt = new MultiFormatReader().decode(binaryBitmap);
        return rslt.getText();
    }

    public void deleteItem(QRItem item) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Delete Item");
        alert.setHeaderText("Delete item: " + item.getName() + "?");
        alert.setContentText("Are you sure? Press OK to confirm, or cancel to abort.");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            QRData.getInstance().deleteQrItem(item);
        }
    }

}
