package com.lato.datamodel;

public class Organization {

    private String name;
    private String address;
    private String website;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Organization(String name, String address, String website) {
        this.name = name;
        this.address = address;
        this.website = website;
    }

    @Override
    public String toString() {
        return  "Name: " + name +
                ", Address: " + address +
                ", Website: " + website;
    }
}
