package com.lato.datamodel;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.FileChooser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;

public class QRData {

    private static QRData instance = new QRData();
//    private static String filename = "data.txt";

    private ObservableList<QRItem> qrItems;

    public static QRData getInstance() {
        return instance;
    }

    public ObservableList<QRItem> getQrItems() {
        return qrItems;
    }

    public boolean openFromFile() throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select separated file");
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT Files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showOpenDialog(null);

        if (file != null) {
            flush();
            loadQRItems(file);
            return true;
        }
        return false;
    }

    public void addQrItem(QRItem item) {
        if(qrItems == null) {
            qrItems = FXCollections.observableArrayList();
        }
        qrItems.add(item);
    }

    public void flush() {
        if(qrItems != null) {
        qrItems.removeAll(getQrItems());
        }
    }

    public void loadQRItems(File file) throws IOException {
            qrItems = FXCollections.observableArrayList();
            Path path = Paths.get(file.getPath());
            BufferedReader br = Files.newBufferedReader(path);

            String input;
            try {
                while ((input = br.readLine()) != null) {
                    String[] itemPieces = input.split(";");

                    String name = itemPieces[0];
                    String surname = itemPieces[1];
                    String title = itemPieces[2];
                    String tel1 = itemPieces[3];
                    if(tel1.equals("")) {
                        tel1 = "Brak";
                    }
                    String tel2 = itemPieces[4];
                    if(tel2.equals("")) {
                        tel2 = "Brak";
                    }
                    String email = itemPieces[5];

                    QRItem qrItem = new QRItem(name, surname, title, tel1, tel2, email);
                    qrItems.add(qrItem);
                }
            } finally {
                if(br != null) {
                    br.close();
                }
            }

        }

    public void storeQrItems(String pathToSave) throws IOException {
        Path path = Paths.get(pathToSave);
        BufferedWriter bw = Files.newBufferedWriter(path);

        try {
            Iterator<QRItem> iterator = qrItems.iterator();
            while (iterator.hasNext()) {
                QRItem qrItem = iterator.next();
                if(qrItem.getTel1().equals("Brak")) {
                    qrItem.setTel1("");
                }
                if(qrItem.getTel2().equals("Brak")) {
                    qrItem.setTel2("");
                }
                bw.write(String.format("%s;%s;%s;%s;%s;%s;",
                        qrItem.getName(),
                        qrItem.getSurname(),
                        qrItem.getTitle(),
                        qrItem.getTel1(),
                        qrItem.getTel2(),
                        qrItem.getEmail()));
                bw.newLine();
            }
        } finally {
            if(bw != null) {
                bw.close();
            }
        }
    }

    public void deleteQrItem(QRItem qrItem) {
        qrItems.remove(qrItem);
    }

}
