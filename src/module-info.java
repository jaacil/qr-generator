module QRgenerator {
    requires javafx.fxml;
    requires javafx.controls;
    requires com.google.zxing;
    requires com.google.zxing.javase;
    requires java.desktop;
    requires java.logging;

    opens com.lato;
}